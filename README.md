Zimmermann was founded by sisters Nicky and Simone Zimmermann in Sydney, Australia. The Ready-to-Wear and Swim & Resort collections embrace relaxed femininity, optimism, and effortless sophistication.

Address : 8468 Melrose Place, West Hollywood, California 90069

Phone : 323-746-5456
